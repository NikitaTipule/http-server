import os
import sys
from response import CommonResponse
from logger import Logger
from config import *
import pathlib
sys.path.append(os.path.abspath(os.path.join('..')))
Rootpath = str(pathlib.Path().absolute()) + "/filesForTesting"

def SeperateHeaderBody(req):
    req_headers = {}
    req_body = []
    for i in req[1:]:
        if(":" in i):
            headerFieldNames = i[:i.index(':')]
            req_headers[headerFieldNames] = i[i.index(':') + 2:len(i) - 1]
        elif ("------" in i):
            index_no = req.index(i)
            req_body = req[index_no:]
            return req_headers, req_body
        else:
            if(i != "\r" and i != "\n" and i != "\r\n" and i != ""):
                req_body.append(i)

    return req_headers, req_body

def extractFormDataFromBody(content_type, req_body):
    form_data = {}
    if("multipart/form-data" in content_type):
        boundary = content_type[content_type.find("=") + 1:]
        key = ''
        value = ''
        try:
            for line in req_body[1:]:
                if('----' in line):
                    form_data[key] = value
                    key = ''
                    value = ''
                elif('Content-Disposition: form-data' in line):
                    key = line[line.index('=') + 2:-1]
                    value = req_body[req_body.index(line) + 2][:]
                else:
                    pass
        except:
            print("error :(")
    elif("application/x-www-form-urlencode" in content_type):
        # req_body.remove('')
        form_data = {}
        for j in req_body:
            pair = j.split('&')
            for k in pair:
                k = k.split('=')
                form_data[k[0]] = k[1]
    else:
        pass

    return form_data


# put method requests to store data at supplied request uri. if request uri is already exist then now it should be modified version
def parse_PUT_Request(clientAddr, headers):

    logger = Logger();
    logger.clientAddress = clientAddr

    req_headers = {}
    req_body = []
    length = 0
    content_type = ""

    req_headers, req_body = SeperateHeaderBody(headers)
    path = headers[0].split(" ")[1]

    
    if(path == "/"):
        path = "index.html"
    else:
        path = Rootpath + path

    # check if the file is exist if not then create it
    if(os.path.exists(path)):
        # check if the file is writable of not
        if(os.access(path, os.W_OK)):
            f = open(path, 'w')
            status_code = 200
        else:
            #forbidden
            status_code = 403
    else:
        # file does not exist so creat it
        f = open(path, "w")
        status_code = 201
    if(status_code == 403):
        response = CommonResponse(403, 0)
        return response, ""

    try:
        content_type = req_headers['Content-Type']
        content_length = req_headers["Content-Length"]
    except: 
        pass

    if(content_type != ""):
        for i in req_body:
            length = length + len(i)
        form_data = extractFormDataFromBody(content_type, req_body)
        f.write(str(form_data))
        f.close()
    else:
        pass
    response = CommonResponse(status_code, length)
    logger.CreateAccessLogRecord(headers[0], response)

    return response, ""

