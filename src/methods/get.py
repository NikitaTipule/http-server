import os
import sys
import gzip
import zlib
import brotli
import datetime
import time
import pathlib
sys.path.append(os.path.abspath(os.path.join('..')))
from response import CommonResponse
from response import generateGetResponse
from logger import Logger
import mimetypes
from config import *
Rootpath = str(pathlib.Path().absolute()) + "/filesForTesting/"

# Parse the accept req_header and return array of all accepted  media type
def parseAccept_req_headers(accept):
    accept_req_header = []
    qValue = []
    accept = accept.split(",")
    for i in accept:
        j = i.split(";")
        accept_req_header.append(j[0])
    return accept_req_header, qValue


# It Parse the get request and according to the request create response return response with message body
def parse_GET_Request(clientAddr, headers, method):

    logger = Logger();
    logger.clientAddress = clientAddr
    Cookie = None

    req_headers = {}
    for i in headers[1:]:
        try:
            headerFieldNames = i[:i.index(':')]
            req_headers[headerFieldNames] = i[i.index(':') + 2:len(i)]
        except:
            pass

    path = headers[0].split(' ')[1]
    if(path == "/"):
        path = "/index.html"
    
    path = Rootpath + path 
    length = 0

    # check condition for bad reques
    if(headers[len(headers)- 1] != ""):
        if(path == "/"):
            path = "index.html"
        else:
            path = Rootpath + path 
        f1 = open(path, "rb")
        global msg_body
        msg_body = f1.read()
        lastModified = os.path.getmtime(path) #returns time of last modification of the specific path
        f1.close()
        length = len(msg_body)
        content_type = mimetypes.guess_type(path, strict=True)
        response = generateGetResponse(400, 0, None, content_type);
        return response, msg_body

    if("Accept" in req_headers.keys()):
        accept = req_headers["Accept"]
        accept_req_headers, qValue = parseAccept_req_headers(req_headers["Accept"])
 
    if("Cookie" in req_headers.keys()):
        Cookie = req_headers["Cookie"]

    content_type = ""
    typeOfFile = mimetypes.guess_type(path, strict=True)

    # 406 not acceptable
    if("*/*" not in accept_req_headers and typeOfFile[0] not in accept_req_headers):
        content_type = typeOfFile[0]
        response = generateGetResponse(406, 0, None, content_type)
        return response, ""

    content_type = typeOfFile[0]

    try:

        f1 = open(path, "rb")
        msg_body = f1.read()
        lastModified = os.path.getmtime(path) #returns time of last modification of the specific path
        f1.close()
        length = len(msg_body)

        # generaitng etag
        etag = str(int(lastModified)) + str(length)

        # Implimentation of If-None-Match header - check if etag is same with if-none-match header value and then response accordingly
        if("If-None-Match" in req_headers.keys()):
            if(etag == req_headers["If-None-Match"]):
                response = generateGetResponse(304, 0, etag, content_type)
                logger.CreateAccessLogRecord(headers[0], response)
                return response, ""

        # not supported so responce is with 415 status code
        if("Content-Encoding" in req_headers.keys() and req_headers["Content-Encoding"] not in ["gzip", "deflate", "br"]):
            response = generateGetResponse(415, 0, etag, content_type)
            logger.CreateErrorLog(headers[0], response)
            return response, ""

        response = generateGetResponse(200, length, etag, content_type, Cookie)

        # IF-Modified-Since header implimentation
        if("If-Modified-Since" in req_headers.keys()):
            months = {
                 "Jan":1, "Feb":2, "Mar":3, "Apr":4, "May":5, "Jun":6, "Jul":7, "Aug":8, "Sep":9, "Oct":10, "Nov":11, "Dec":12 
            }
            givendate = req_headers["If-Modified-Since"]
            i = givendate[5:].split(" ")
            j = i[3].split(":")
            day = int(i[0]); month = months[i[1]]; year = int(i[2])
            hr = int(j[0]); min = int(j[1]); sec = int(j[2])
            dtime = datetime.datetime(year, month, day, hr, min, sec)
            giventime = time.mktime(dtime.timetuple())

            if(lastModified > giventime):
                response = generateGetResponse(304, 0, etag, content_type)
                logger.CreateAccessLogRecord(headers[0], response);
                return response, ""

        # Accpet-Encoding header implimentaion
        if("Accept-Encoding" in req_headers.keys()):
            if("gzip" in req_headers["Accept-Encoding"]):
                newresponse = ""
                for i in response.split("\r\n"):
                    if("Content-Length" in i):
                        continue
                    else:
                        newresponse = newresponse + i + "\r\n"
                
                msg_body = gzip.compress(msg_body)
                newresponse = newresponse[:len(newresponse) - 4] + "Content-Length: {}\r\nContent-Encoding: gzip\r\n\r\n".format(str(len(msg_body)))
                response = newresponse
            elif("deflate" in req_headers["Accept-Encoding"]):
                newresponse = ""
                for i in response.split("\r\n"):
                    if("Content-Length" in i):
                        continue
                    else:
                        newresponse = newresponse + i + "\r\n"
                msg_body = zlib.compress(msg_body)
                newresponse = newresponse[:len(newresponse) - 4] + "Content-Length: {}\r\nContent-Encoding: deflate\r\n\r\n".format(str(len(msg_body)))
                response = newresponse
            elif("br" in req_headers["Accept-Encoding"]):
                newresponse = ""
                for i in response.split("\r\n"):
                    if("Content-Length" in i):
                        continue
                    else:
                        newresponse = newresponse + i + "\r\n"
                msg_body = brotli.compress(msg_body)
                newresponse = newresponse[:len(newresponse) - 4] + "Content-Length: {}\r\nContent-Encoding: br\r\n\r\n".format(str(len(msg_body)))
                response = newresponse
    

        # range header implimentaion  
        if("Range" in req_headers.keys()):
            total_bytes = int(req_headers["Range"])
            if(total_bytes >= 0):
                length = len(msg_body)
                if(total_bytes > length):
                    response = generateGetResponse(416, 0, etag)
                    logger.CreateAccessLogRecord(headers[0], response)
                    logger.CreateErrorLog(headers[0], response)
                    return response, ""
                
                new_body = msg_body[:total_bytes]
                newresponse = ""
                for i in response.split("\r\n"):
                    if("Content-Length" in i):
                        continue
                    else:
                        newresponse = newresponse + i + "\r\n"
                newresponse = newresponse[:len(newresponse)-4] + "Content-Length: {}\r\n\r\n".format(len(new_body))
                msg_body = new_body

                response = newresponse
            else:
                length = len(msg_body)
                total_bytes = -total_bytes
                if(total_bytes > length):
                    response = generateGetResponse(416, 0, etag)
                    logger.CreateAccessLogRecord(headers[0], response)
                    logger.CreateErrorLog(headers[0], response)
                    return response, ""

                new_body = msg_body[length - total_bytes:length]
                newresponse = ""
                for i in response.split("\r\n"):
                    if("Content-Length" in i):
                        continue
                    else:
                        newresponse = newresponse + i + "\r\n"
                newresponse = newresponse[:len(newresponse)-4] + "Content-Length: {}\r\n\r\n".format(len(new_body))
                msg_body = new_body

                response = newresponse


        logger.CreateAccessLogRecord(headers[0], response)
        if(method == "GET"):
            return response, msg_body
        if(method == "HEAD"):
            return response, ""
    except FileNotFoundError:
        response = generateGetResponse(404, length)
        logger.CreateErrorLog(headers[0], response)
        logger.CreateAccessLogRecord(headers[0], response)
        return response, ""

