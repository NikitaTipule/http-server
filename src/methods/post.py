import os
import sys
from response import CommonResponse
from logger import Logger
from config import *
import pathlib
sys.path.append(os.path.abspath(os.path.join('..')))
Rootpath = str(pathlib.Path().absolute()) + "/filesForTesting"

# Seperate headers and request body
def SeperateHeaderBody(req):
    req_headers = {}
    req_body = []
    for i in req[1:]:
        if(":" in i):
            headerFieldNames = i[:i.index(':')]
            req_headers[headerFieldNames] = i[i.index(':') + 2:len(i)]
        elif ("------" in i):
            index_no = req.index(i)
            req_body = req[index_no:]
            return req_headers, req_body
        else:
            if(i != "\r" and i != "\n" and i != "\r\n" and i != ""):
                req_body.append(i)

    return req_headers, req_body

# extract data from body of the request
def extractFormDataFromBody(content_type, req_body):
    form_data = {}
    if("multipart/form-data" in content_type):
        boundary = content_type[content_type.find("=") + 1:]
        key = ''
        value = ''
        print(boundary)
        try:
            for line in req_body[1:]:
                if('----' in line):
                    form_data[key] = value
                    key = ''
                    value = ''
                elif('Content-Disposition: form-data' in line):
                    key = line[line.index('=') + 2:-1]
                    value = req_body[req_body.index(line) + 2][:]
                else:
                    pass
        except:
            print("error :(")
    elif("application/x-www-form-urlencoded" in content_type):
        form_data = {}
        for j in req_body:
            pair = j.split('&')
            for k in pair:
                k = k.split('=')
                try:
                    form_data[k[0]] = k[1]
                except:
                    pass
    else:
        pass

    return form_data


# It Parse the post request and according to the request create response return response
def parse_POST_Request(clientAddr, headers):
    #for this project, the data from post request i am storing in post_log.txt file which is in logs folder

    logger = Logger();
    logger.clientAddress = clientAddr

    req_headers = {}
    req_body = []
    length = 0
    content_type = ""

    req_headers, req_body = SeperateHeaderBody(headers)
    path = headers[0].split(" ")[1]  

    if(path == "/"):
        path = "index.html"
    else:
        path = Rootpath + path

    # check if the file is exist if not then create it
    if(os.path.exists(path)):
        # check if the file is writable of not
        f = open(path, "a")
        if(f.writable()):
            status_code = 200
        else:
            #forbidden
            status_code = 403
        status_code = 200
        f.close()

    else:
        # file does not exist so creat it
        f = open(path, "w")
        f.close()
        status_code = 201
    if(status_code == 403):
        response = CommonResponse(403, 0)
        return response, ""

    try:
        content_type = req_headers['Content-Type']
        content_length = req_headers["Content-Length"]
    except: 
        pass

    if(content_type != ""):
        for i in req_body:
            length = length + len(i)
        form_data = extractFormDataFromBody(content_type, req_body)
        logger.CreatePostLog(form_data)
    else:
        pass
    response = CommonResponse(status_code, length)
    logger.CreateAccessLogRecord(headers[0], response)

    return response, ""
