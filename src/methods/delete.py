import os
import sys
from response import CommonResponse
from logger import Logger
from config import *
import pathlib
sys.path.append(os.path.abspath(os.path.join('..')))
Rootpath = str(pathlib.Path().absolute()) + "/filesForTesting"

logger = Logger()

# It Parse the delete request and according to the request create response return response
def parse_DELETE_Request(clientAddr, headers):

    req_headers = {}
    req_body = []
    logger.clientAddress = clientAddr

    for i in headers[1:]:
        try:
            headerFieldNames = i[:i.index(':')]
            req_headers[headerFieldNames] = i[i.index(':') + 2:len(i)]
        except:
            pass
    path = headers[0].split(' ')[1]

    if(path == "/"):
        path = "index.html"
    else:
        path = Rootpath + path

    # check if the file is availale or not
    if(os.path.exists(path)):
        # check it is accessable or not (writable)
        if(os.access(path, os.W_OK)):
            os.remove(path)
            response_code = 204
            response = CommonResponse(response_code, 0)
            logger.CreateAccessLogRecord(headers[0], response)
            return response, ""
        else:
            #forbidden - not permitted
            response_code = 403
            response = CommonResponse(response_code, 0)
            logger.CreateErrorLog(headers[0], response)
            logger.CreateAccessLogRecord(headers[0], response)
            return response, ""
    else:
        # file not found
        response_code = 404
        response = CommonResponse(response_code, 0)
        logger.CreateAccessLogRecord(headers[0], response)
        logger.CreateErrorLog(headers[0], response)
        return response, ""

    