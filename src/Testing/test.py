import sys
import requests
import time
import threading
import datetime
from prettytable import PrettyTable 
from rich.table import Table
from rich import box
from rich.console import Console
from rich import print
import pytz
import pathlib
import os
import socket

try:
    port = sys.argv[1]
except:
    port = 9999
url = "http://localhost:{}/".format(port)

totalOptions = len(sys.argv)

# Options for testing: 
# -g for get
# -po for post
# -pu for put
# -d for delete
# -h for head
# -c for cookies
# -m for multithreading

options = []
for i in range(0, totalOptions - 2):
    options.append(sys.argv[i + 2])

def PrintTable(tableTitle, response, expected_code):
    table = Table(show_header= True, header_style="bold cyan", title=tableTitle)
    table.add_column("Method", style="bold yellow")
    table.add_column("Response", style="green")
    table.add_column("Message", style="blue")
    table.add_column("Result")
    code = response.status_code
    if (code in expected_code):
        result = '[green]Success'
    else:
        result = '[red]Failed'
    table.add_row(response.request.method, str(response.status_code), response.reason, result)
    print(table)

def TestGet(url):

    # files available
    print("")
    diffFiles = [
        "test.pdf", "test.jpeg", "nikita.html", "index.html", "test.js", "test.css", "cookiepath.html", "Hello.html"
    ]
    for i in diffFiles:
        response = requests.get(url + i)
        if (response.status_code == 404):
            PrintTable('[cyan]Testing get', response, [404])
        else:
            PrintTable('[cyan]Testing get', response, [200])
        print('Body Length: ', len(response.text))
        print("\n")

    TestBadAccept(url)
    print("-"* 40)
    print("\n")
    TestContentEncoding(url)
    print("-"* 40)
    print("\n")
    TestRange(url)
    print("-"* 40)
    print("\n")
    TestConditionalGet(url)


# Test Bad Accept
def TestBadAccept(url):
    response = requests.get(url, headers= {"Accept":"NotExist"})
    PrintTable('[cyan]Testing Bad Accept', response, [406])

def TestRange(url):
    response = requests.get(url, headers={"Range": "100"})
    PrintTable("[cyan]Testing Range", response, [200])
    print('Body Length : ', len(response.text))
    print("\n")
    
    response = requests.get(url, headers={"Range": "100000"})
    PrintTable("[cyan]Testing Bad Range", response, [416])
    print('Body Length : ', len(response.text))

def TestConditionalGet(url):
    response = requests.get(url, headers={"If-None-Match": "1636783890623732"})
    PrintTable("[cyan]Testing Conditional Get - If-None-Match", response, [200, 304])
    print('Body Length : ', len(response.text))
    print("\n")

    response = requests.get(url, headers={"If-None-Match": "168794512365"})
    PrintTable("[cyan] Testing Conditional Get : If-None-Match", response, [200])
    print("Body Length : ", len(response.text))
    print("\n")

    # dtime = datetime.datetime(year, month, day, hr, min, sec)
    date = datetime.datetime(2021, 11, 13, 9, 30, 30)
    # print(date)
    time = "{}:{}:{} GMT".format(date.strftime("%H"), date.strftime("%M"), date.strftime("%S"))
    date = date.strftime("%a") + ", " + str(date.strftime("%d")) + " " + date.strftime("%b") + " " + str(date.strftime("%Y")) + " " + time
    # print(date)
    response = requests.get(url, headers={"If-Modified-Since": date})
    PrintTable("[cyan] Testing Conditional Get : If-Modified-Since", response, [304])
    print("Body Length : ", len(response.text))
    print("\n")

    # date = datetime.datetime(2021, 5, 13, 9, 30, 30)
    # # print(date)
    # time = "{}:{}:{} GMT".format(date.strftime("%H"), date.strftime("%M"), date.strftime("%S"))
    # date = date.strftime("%a") + ", " + str(date.strftime("%d")) + " " + date.strftime("%b") + " " + str(date.strftime("%Y")) + " " + time
    # # print(date)
    # response = requests.get(url, headers={"If-Modified-Since": date})
    # PrintTable("[cyan] Testing Conditional Get : If-Modified-Since", response, [200])
    # print("Body Length : ", len(response.text))
    # print("\n")


def TestContentEncoding(url):
    response = requests.get(url, headers = {"Accept-Encoding": "deflate"})
    PrintTable("[cyan]Testing Content Encoding - deflate", response, [200])
    print("\n")

    response = requests.get(url, headers = {"Accept-Encoding": "gzip"})
    PrintTable("[cyan]Testing Content Encoding - gzip", response, [200])
    print("\n")

    response = requests.get(url, headers = {"Accept-Encoding": "br"})
    PrintTable("[cyan]Testing Content Encoding - br", response, [200])
    print("\n")

    response = requests.get(url)
    PrintTable("[cyan]Testing Content Encoding - no", response, [200])

def TestHead(url):
    # files available
    print("")
    diffFiles = [
        "test.pdf", "test.jpeg", "nikita.html", "index.html", "test.js", "test.css", "cookiepath.html", "Hello.html"
    ]
    for i in diffFiles:
        response = requests.head(url + i)
        if (response.status_code == 404):
            PrintTable('[cyan]Testing head', response, [404])
        else:
            PrintTable('[cyan]Testing head', response, [200])
        print('Body Length: ', len(response.text))

def TestDelete(url):
    path = os.getcwd()
    path = os.path.dirname(path) + "/filesForTesting/"
    f = open(path + "fortest.txt", "w")
    f.close()

    response = requests.delete(url + "fortest.txt")
    PrintTable("[white underline]Testing Delete", response, [204])

    response = requests.delete(url + "/helloo.html")
    PrintTable("[white underline]Testing Delete with FileNotFound", response, [404])

def TestPost(url):
    msg_body = {"name" : "Nikita", "mis":"111903051"}
    response = requests.post(url + "/nikita.html", data = msg_body)
    PrintTable("[cyan underline]Testing Post", response, [200, 201, 204, 403])

def TestPut(url):

    path = os.getcwd()
    path = os.path.dirname(path) + "/filesForTesting/"
    f = open(path + "forPut.txt", "w")
    f.write("hello")
    f.close()

    msg_body = {"name" : "Nikita", "mis" : "111903051"}
    response = requests.put(url + "/forPut.txt", data = msg_body)
    PrintTable("[cyan underline]Testing Put", response, [200, 201, 204])

    msg_body = {"name" : "Nikita", "mis" : "111903051"}
    response = requests.put(url + "/PutTesting.txt", data = msg_body)
    PrintTable("[cyan underline]Testing Put", response, [200, 201, 204])
    
# def TestHttpVersionNotMatch():
def TestCookie(url):
    print("Sending request first time : ")
    response = requests.get(url + "cookiepath.html")
    cookie = response.headers["Set-Cookie"]
    print("Cookie id from server : ", cookie)

    print("Sending request second time with the cookie received form server")
    response = requests.get(url + "cookiepath.html", headers = {"Cookie" : cookie})
    print("received cookie : ", response.headers["Cookie"])
    print("Status Code : ", response.status_code)


def send_request(url, table):
    response = requests.get(url, headers= {"Accept-Encoding": ""})
    code = response.status_code
    if (code in [200]):
        result = '[green]Success'
    else:
        result = '[red]Failed'
    table.add_row(response.request.method, str(response.status_code), response.reason, result)
    time.sleep(10)
    return

def Testmultithreading(url):
    table = Table(show_header= True, header_style="bold cyan", title="Testing Multithreading")
    table.add_column("Method", style="bold yellow")
    table.add_column("Response", style="green")
    table.add_column("Message", style="blue")
    table.add_column("Result")
    i = 15
    while(i):
        thread1 = threading.Thread(target=send_request, args=(url, table,  )).start()
        i = i - 1
    print(table)

   

if __name__ == "__main__":
    for i in options:
        if(i == "-g"):
            TestGet(url)
            print("-" * 50)
            print("\n\n")
        elif(i == "-po"):
            TestPost(url)
            print("-" * 50)
            print("\n\n")
        elif(i == "-pu"):
            TestPut(url)
            print("-" * 50)
            print("\n\n")
        elif(i == "-h"):
            TestHead(url)
            print("-" * 50)
            print("\n\n")
        elif(i == "-d"):
            TestDelete(url)
            print("-" * 50)
            print("\n\n")
        elif(i == "-c"):
            TestCookie(url)
            time.sleep(16)
            print("\nSending cookie again after previous cookie is expired")
            TestCookie(url)
            print("-" * 50)
            print("\n\n")
        elif(i == "-m"):
            Testmultithreading(url+ "1.html")
            print("-" * 50)
            print("\n\n")
        else:
            print("\nInvalid Option : ", i)
            print("\n")
    