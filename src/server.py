import sys
import os
import socket
import threading
from response import CommonResponse
from methods.get import parse_GET_Request
from methods.post import parse_POST_Request
from methods.delete import parse_DELETE_Request
from methods.put import parse_PUT_Request
from methods.head import parse_HEAD_Request
from logger import Logger
import signal
from config import *


msg_body = None
ConnectedClients = []

def process(data, clientAddr, received_msg = None):

    headers = data.split("\r\n")
    start_line = headers[0].split(' ')

    # Check Http Version
    if(start_line[2].split("/")[1] != "1.1"):
        response = CommonResponse(505, 0)
        return response, ""

    method = start_line[0]
    global msg_body
    if(method == 'GET'):
        return parse_GET_Request(clientAddr, headers, "GET")
    elif(method == "POST"):
        return parse_POST_Request(clientAddr, headers)
    elif(method == "HEAD"):
        return parse_HEAD_Request(clientAddr, headers, "HEAD")
    elif(method == "PUT"):
        return parse_PUT_Request(clientAddr, headers)
    elif(method == "DELETE"):
        return parse_DELETE_Request(clientAddr, headers)
    return 

# get the connection type
def GetConnectionType(res):
    res = res.split("\r\n")
    for i in res[1:]:
        try:
            headerFieldNames = i[:i.index(':')]
            if(headerFieldNames == "Connection"):
                return i[i.index(':')+2:len(i)]
        except:
            pass

#creates persistent tcp connection and when connection timeouts then it will close it
def ClientSocketThread(clientSocket, clientAddr):
    global ConnectedClients
    if(Persistent):
        clientSocket.settimeout(10)
        while 1:
            try:
                received_msg = clientSocket.recv(1000000)
                data = received_msg.decode('ISO-8859-1')

                if(not data):
                    break
                res, msg_body = process(data, clientAddr, received_msg)
                clientSocket.send(res.encode('utf-8'))
                if(msg_body):
                    clientSocket.send(msg_body)
                connectionType = GetConnectionType(res)
                if(connectionType == "close"):
                    clientSocket.close()
                    break
            except socket.timeout:
                clientSocket.close()
                break
    else:
        while 1:
            try:
                received_msg = clientSocket.recv(8388608)
                data = received_msg.decode('ISO-8859-1')
                if(not data):
                    break
                res, msg_body = process(data, clientAddr, received_msg)
                clientSocket.send(res.encode('utf-8'))
                if(msg_body):
                    clientSocket.send(msg_body)
                clientSocket.close()
                break
            except:
                pass

    ConnectedClients.remove((clientSocket, clientAddr))
    return

# to stop the server
def stopserver(signal, frame):
    s.close()
    sys.exit(1)


if __name__ == "__main__":
    logger = Logger()
    try:
        signal.signal(signal.SIGINT, stopserver)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(('127.0.0.1', PORT))
        s.listen(MAX_CONNECTIONS)
        print("listening on the port : ", PORT)
        
        while 1:
            if(len(ConnectedClients) >= MAX_CONNECTIONS):
                print("Reached to the maximum limit .... Try again after some time")
                continue
            clientSocket, clientAddr = s.accept()
            ConnectedClients.append((clientSocket, clientAddr))
            thread = threading.Thread(target = ClientSocketThread, args = (clientSocket, clientAddr, ))
            thread.start()
    except Exception as e:
        print("Some error has occured while creating socket")
        logger.ServerError(e)
        sys.exit(1)


# SIGINT is the interrupt signal (ctrl+C). Its default behaviour is to terminate the process.
