from config import *

class Logger():

    clientAddress = ''

    # create Access log record and store in a file whose path is given in confog file
    def CreateAccessLogRecord(self, req, res):
        try:
            f = open(logPath, "a")
            res = res.split("\r\n")
            res_headers = {}

            for i in res[1:]:
                try:
                    headerField = i[:i.index(':')]
                    res_headers[headerField] = i[i.index(':') + 2:len(i)]
                except:
                    pass

            date = res_headers["Date"].split(' ')
            resp_line = res[0]
            log = self.clientAddress[0] + "  " + "[" + date[1] + "/" + date[2] + "/" + date[3] + " " + date[4] + "]" + ' "' + req + '" ' + resp_line[resp_line.index(" "):]
            if("Content-Length" in res_headers.keys()):
                log = log + " " + res_headers["Content-Length"]
            f.write(log + "\n")
            f.close()
        except:
            print("error in access logs")

    #create error log record and store in a file whose path is given in confog file
    def CreateErrorLog(self, req, res):

        f = open(errorPath, "a")
        try:
            res = res.split("\r\n")
            res_headers = {}

            for i in res[1:]:
                try:
                    headerField = i[:i.index(':')]
                    res_headers[headerField] = i[i.index(':') + 2:len(i) - 1]
                except:
                    pass

            date = res_headers["Date"].split(' ')
            resp_line = res[0]
            log = self.clientAddress[0] + "  " + "[" + date[1] + "/" + date[2] + "/" + date[3] + " " + date[4] + "]" + ' "' + req + '" ' + resp_line[resp_line.index(" "):]
            if("Content-Length" in res_headers.keys()):
                log = log + " " + res_headers["Content-Length"]

        except:
            log = "Bad request [{}]".format(res)
        f.write(log + "\n")
        f.close()

    #create post log record and store in a file whose path is given in confog file
    def CreatePostLog(self, data):
        try:
            f = open(PostPath, "a")
            # print(str(data))
            f.write(str(data))
            f.write("\n")
            f.close()
        except:
            print("Error in post logs")

    def CreatePutLog(self, data):
        try:
            f = open(PutPath, "w")
            f.write(str(data));
            f.write("\n")
            f.close()
        except:
            print("Error in put log")

    #create server error log record and store in a file whose path is given in confog file
    def ServerError(self, e):
        try:
            f = open(ServerErrorPath, "a")
            f.write(str(e))
            f.write("\n");
            f.close()
        except:
            print("Error in serverError log")


#Apache2 logger file is in /var/logs/apache2

# Two types of logs
# 1. Access logs => keeps track of all the access requests made to the wens server and the servers responses to those requests
# 2. error logs => keeps track of all the errors encountered during request responnnnce cycle and in general keeps track of the server

# Common format of a log file:

# 1. %>s HTTP status code for a request

# 2. %U URL of the specified resource

# 3. %a IP address of the client

# 4. %T Time required to make the request

# 5. %t Timestamp either in RFC 1123/1036/ANSI C time format


# Decide on a specified format that is read from the config file of the server. 
#Essentially keep two levels of logging => One for the access and the other for the errors like apach

# 127.0.0.1 - - [25/Oct/2021:15:56:51 +0530] "GET / HTTP/1.1" 200 3477 "-" "Mozilla/5.0 (X11; Linux x86_64) 
# AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36"

# LogFormat "%h %l %u %t \"%r\" %>s %b" common
# CustomLog logs/access_log common


