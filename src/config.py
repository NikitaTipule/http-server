import pathlib

# Default Port
PORT = 9999

# Data size to receive from client at start
SIZE = 4096

Absolute_path = str(pathlib.Path().absolute())

# Path of Root Directory
Rootpath = Absolute_path + "/filesForTesing"

# maximum simultenious connetions
MAX_CONNECTIONS = 20

# for perisstent connection, Persistent = True,
# fot non-Persistent connection , Persistent = False
Persistent = False


# Paths of file to store log messages
logPath = "./logs/access_log.txt"
errorPath = "./logs/error_log.txt"
PostPath = "./logs/post_log.txt"
PutPath = "./logs/put_log.txt"
ServerErrorPath = "./logs/servererror_log.txt"
