import datetime
import pytz
import random
from config import *

status = {
    101 : "Switching Protocols",
    200 : "OK",
    201 : "Created",
    202 : "Accepted",
    203 : "Non-Authoritative Information",
    204 : "No Content",
    205 : "Reset Content",
    206 : "Partial Content",
    300 : "Multiple Choices",
    301 : "Moved Permently",
    302 : "Found",
    303 : "See Other",
    304 : "Not Modified",
    305 : "Use Proxy",
    307 : "Temporary Redirect",
    400 : "Bad Request",
    401 : "Unauthorized",
    402 : "Payment Required",
    403 : "Forbidden",
    404 : "Not Found",
    405 : "Method Not Allowed",
    406 : "Not Acceptable",
    407 : "Proxy Authentication Required",
    408 : "Request Time-out",
    409 : "Conflict",
    410 : "Gone",
    411 : "Length Required",
    412 : "Precondition Failed",
    413 : "Request Entity Too Large",
    414 : "Request-URI Too Large",
    415 : "Unsupported Media Type",
    416 : "Requested range not satisfiable",
    417 : "Expectation Failed",
    500 : "Internal Server Error",
    501 : "Not Implemented",
    502 : "Bad Gateway",
    503 : "Service Unavailable",
    504 : "Gateway Time-out",
    505 : "HTTP Version not supported"
}

# response = status_line + general header + response header + entity header + msg body

# Function to generate response
def CommonResponse(status_code, length, msg_body="", lastModified = None, ctype="text/html", encoding="gzip"):
    if(status_code not in status.keys()):
        return
    date = datetime.datetime.now(tz=pytz.utc)
    time = "{}:{}:{} GMT".format(date.strftime("%H"), date.strftime("%M"), date.strftime("%S"))
    date = date.strftime("%a") + ", " + str(date.strftime("%d")) + " " + date.strftime("%b") + " " + str(date.strftime("%Y")) + " " + time
    status_line = "HTTP/1.1 {} {}\r\n".format(status_code, status[status_code])
    responseheader = "Server: My-HTTP-Server\r\n"
    if(Persistent):
        responseheader += "Connection: keep-alive\r\n"
    else:
        responseheader += "Connection: close\r\n"
    entityheader =  "Date: {}\r\nContent-Type: {}; charset=UTF-8\r\nContent-Length :{}\r\n\r\n".format(date, ctype, length)
    response = status_line + responseheader + entityheader
    return response

def generateGetResponse(status_code, length, etag=None, content_type=None, Cookie=None):
    if(status_code not in status.keys()):
        return
    date = datetime.datetime.now(tz=pytz.utc)
    time = "{}:{}:{} GMT".format(date.strftime("%H"), date.strftime("%M"), date.strftime("%S"))
    date = date.strftime("%a") + ", " + str(date.strftime("%d")) + " " + date.strftime("%b") + " " + str(date.strftime("%Y")) + " " + time
    status_line = "HTTP/1.1 {} {}\r\n".format(status_code, status[status_code])
    responseheader = "Server: My-HTTP-Server\r\n"
    
    if(Cookie):
        responseheader = responseheader + "Cookie: " + Cookie + "\r\n";
    else:
        Cookie = str(random.randint(10000,50000))
        path = "/cookiepath.html"
        domain = ""
        responseheader = responseheader + "Set-Cookie: " + "cookieid=" + Cookie + "; "+"Max-Age=30; Path={}; Domain={}\r\n".format(path, domain) 
    responseheader += "Accept-Ranges: bytes\r\n"
    if(Persistent):
        responseheader += "Connection: keep-alive\r\n"
    else:
        responseheader += "Connection: close\r\n"
    if(etag):
        responseheader += "Etag: {}\r\n".format(etag)
    entityheader =  "Date: {}\r\nContent-Type: {}; charset=UTF-8\r\nContent-Length: {}\r\nContent-Language: en-US\r\n\r\n".format(date, content_type, length)
    response = status_line + responseheader + entityheader
    return response
