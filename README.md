# http-server

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://github.com/NikitaTipule/http-server">
    <img src="./src/image/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">HTTP/1.1 Server</h3>
    <p align="center">
    Implemented HTTP/1.1 an application-level protocol based on RFC 2616.    
</p>
  
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#author">Author</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

**HTTP/1.1 protocol** implementation. HTTP has messages which consist of requests from client to server and responses from server to client. Query consist different methods, status codes, headers, MIME-types etc.

- **Methods Handled**

  - GET
  - POST
  - HEAD
  - PUT
  - DELETE

- **Status Codes Handled**

  - 200 : OK
  - 201 : Created
  - 202 : Accepted
  - 204 : No Content
  - 304 : Not Modified
  - 400 : Bad Request
  - 403 : Forbidden
  - 404 : Not Found
  - 406 : Not Acceptable
  - 416 : Requested range not satistfied
  - 505 : HTTP Version Not Supported

- **Features**

  1. Multithreaded Server
  2. Config file - to store server configuration
  3. Cookies
  4. Logging
  5. Automated Testing of all options
  6. Persistent/Non-Persistent Server

  **Note**: To check for the history of requests made to server, refer data.log in htdocs folder.

<!-- GETTING STARTED -->

## Getting Started

1. Clone the repo.
2. Navigate to the src directory.

**Note**: Make sure python3 is supported.

<!-- USAGE EXAMPLES -->

## Usage

1. Run server by:

   **Usage:**
   **_ To start the server _**
   ./start.sh
   It will start the server in the background at the port specified in config file.

   **_ To stop the server _**
   ./stop.sh

   **_ To Restart the Server _**
   ./restart.sh

2. To run the Test Suite:

   **Usage:** python3 test.py <port_no> [Options]

**Note**: Make sure that the port number is same i.e where the server is running(Port number is in Config file). Also before running this file, make sure server is started.

<!-- CONTACT -->

## Author

[Nikita Tipule](https://github.com/NikitaTipule)

<!-- ACKNOWLEDGEMENTS -->

## Acknowledgements

- [RFC 2616](https://datatracker.ietf.org/doc/html/rfc2616)
- [COEP FOSS - HTTP SERVER] (http://foss.coep.org.in/coepwiki/index.php/HTTP_Server_Project)
- [PYTHON DOC](https://docs.python.org/3/library/http.server.html)
- [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/HTTP)
